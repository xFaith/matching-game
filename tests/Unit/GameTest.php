<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Backend\Models\Game;

class GameTest extends TestCase
{
	// use RefreshDatabase;
    /**
     * A basic unit test example.
     *
     * @return void
     */

    public function testGame()
    {
        $res = Game::first();
        $this->assertArrayHasKey('deck', $res);
        $this->assertEquals($res['deck'], 12);
    }

    public function testCreateCard()
    {
        $game = Game::first();
        $res = $game->createCard();
        $this->assertFalse(empty($res));
        $this->assertIsArray($res);
        $this->assertEquals(count($res), $game->deck);
    }

    public function testIsEvenNumber()
    {
        $game = Game::first();
        $res = $game->isEvenNumber();
        $this->assertTrue($res);

        $game->deck = 13;
        $res = $game->isEvenNumber();
        $this->assertFalse($res);

        $game->deck = 'test';
        $res = $game->isEvenNumber();
        $this->assertFalse($res);
    }

    public function testHistoriesByUserId()
    {
        $game = Game::first();
        $res = $game->historiesByUserId(1)->first();
        $this->assertEquals($res['game_id'], $game->id);
        $this->assertEquals($res['user_id'], 1);

    }

    public function testUserMinFlip()
    {
        $game = Game::first();
        $res = $game->userMinFlip(1);

        $items = $game->historiesByUserId(1)->get()->toArray();
        $rand_idx = $items ? array_rand($items) : 0;
        $this->assertIsInt($res);
        $this->assertLessThanOrEqual($items[$rand_idx]['flips'], $res);
    }

    public function testGlobalMinFlip()
    {
        $game = Game::first();
        $res = $game->globalMinFlip(1);

        $items = $game->histories->toArray();
        $rand_idx = $items ? array_rand($items) : 0;
        $this->assertIsInt($res);
        $this->assertLessThanOrEqual($items[$rand_idx]['flips'], $res);
    }

    public function testCheckCardMatch()
    {
        $game = Game::first();
        $cards = $game->createCard();
        sort($cards);
        $game->card_1 = 0;
        $game->card_2 = 1;
        $res = $game->checkCardMatch($cards);
        $this->assertTrue($res);
        $game->card_2 = 11;
        $res = $game->checkCardMatch($cards);
        $this->assertFalse($res);
    }
}
