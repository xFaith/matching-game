<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;
use Backend\Models\User;

class RouteTest extends TestCase
{
    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testRoute()
    {
        $res = $this->get('api/games/1');
        $res->assertStatus(200);

        $res = $this->get('api/games/1/new_game');
        $res->assertStatus(200);

        $res = $this->get('api/games/1/global_best');
        $res->assertStatus(200);

        $user = User::find(1);
        $res = $this->actingAs($user)->get('api/games/1/my_best');
    }
}
