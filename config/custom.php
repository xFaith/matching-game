<?php

return [
    'api_url' => env('API_URL', 'http://localhost:8000/api'),
    'pusher' => [
        'key' => env('PUSHER_APP_KEY', 'ae5324a810e8cccfb5bc'),
        'cluster' => env('PUSHER_APP_CLUSTER', 'ap1'),
    ],
];
