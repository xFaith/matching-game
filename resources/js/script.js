document.addEventListener("DOMContentLoaded", function() { 
	loadGame();
});

var flipSound = new Audio('assets/frontend/sounds/flip.wav');

var bg = document.getElementsByClassName('background-wrap');

function bgNewGame()
{
	bg[0].classList.remove('visible');
	bg[1].classList.add('visible');
}

function bgEndGame()
{
	bg[0].classList.add('visible');
	bg[1].classList.remove('visible');
}

function loadGame()
{
	let xhr = new XMLHttpRequest();
	xhr.open('GET', `${apiUrl}/games/${gameId}`, true);
	xhr.onload = function() {
		let res = JSON.parse(this.response);
		for (var i = 0; i < res.deck; i++)
			generateCard();
		if(res.global_best)
			document.getElementById('global-best').innerText = res.global_best;
	}
	xhr.send();
}

function generateCard()
{
	const container = document.getElementById('card-container');
	const card = document.createElement('div');
	card.classList.add('card');
	card.innerHTML = `
	<div class="card-back card-face">
	<img class="card-img" src="./assets/frontend/images/logo.jpg">
	</div>
	<div class="card-front card-face">
	<span class="card-value"></span>
	</div>`;
	container.appendChild(card);
}

function ready()
{
	let overlay = document.getElementById('overlay');
	let btn = document.getElementById('new-game');
	let cards = Array.from(document.getElementsByClassName('card'));
	let game = new MatchGame(cards);

	game.busy = true;

	overlay.classList.remove('visible');

	btn.addEventListener('click', () => {
		game.newGame();
	});

	cards.forEach((card, i) => {
		card.addEventListener('click', () => {
			game.flipCard(card, i);
		});
	});
}

function formToJson(form)
{
	return JSON.stringify(Object.fromEntries(new FormData(form)));
}

