
function login()
{
	let form = document.getElementById('login-form');
	let data = formToJson(form);
	let xhr = new XMLHttpRequest();
	xhr.open('POST', `${apiUrl}/login`, true);
	xhr.setRequestHeader('Content-Type','application/json; charset=UTF-8');
	xhr.onreadystatechange = function() {
		if (this.readyState == 4)
		{
			let res = JSON.parse(this.response);
			if (this.status == 200)
				prepare(res.success);
			else
				handleError(0, res.error);
		}
	};
	xhr.send(data);
}

function register()
{
	let form = document.getElementById('register-form');
	let data = formToJson(form);
	let xhr = new XMLHttpRequest();
	xhr.open('POST', `${apiUrl}/register`, true);
	xhr.setRequestHeader('Content-Type','application/json; charset=UTF-8');
	xhr.send(data);
	// let res = xhr.response;
	// console.log(res);
	// prepare(res);
	xhr.onreadystatechange = function() {
		if (this.readyState == 4)
		{
			let res = JSON.parse(this.response);
			if (this.status == 200)
				prepare(res.success);
			else
				handleError(1, res.error);
		}
	};
}

function handleError(idx, res)
{
	let errorMsg;
	if(res.username)
		errorMsg = res.username[0];
	else if(res.password)
		errorMsg = res.password[0];
	else
		errorMsg = 'Invalid username or password';
	let errorBox = document.getElementsByClassName('error-wrap');
	errorBox[idx].innerText = errorMsg;
}


function prepare(res)
{
	document.getElementsByClassName('user-info')[0].innerText = 'Welcome: ' + res.username;
	localStorage.setItem('token', res.token);
	getMyBest();
	ready();
}

function loginForm()
{
	document.getElementsByClassName('register-form')[0].classList.remove('active');
	document.getElementsByClassName('login-form')[0].classList.add('active');
}

function registerForm()
{
	document.getElementsByClassName('login-form')[0].classList.remove('active');
	document.getElementsByClassName('register-form')[0].classList.add('active');
}

function checkLogin()
{
	return localStorage.getItem('token') != null;
}

function getMyBest()
{
	let xhr = new XMLHttpRequest();
	let token = localStorage.getItem('token');
	xhr.open('GET', `${apiUrl}/games/${gameId}/my_best`, true);
	xhr.setRequestHeader('Content-Type','application/json; charset=UTF-8');
	xhr.setRequestHeader('Authorization','Bearer ' + token);
	xhr.onload = function() {
		let res = JSON.parse(this.response);
		if(res.my_best)
			document.getElementById('my-best').innerText = res.my_best;
	};
	xhr.send();
}