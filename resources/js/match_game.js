class AudioController
{
    constructor(cards) {
        this.music = new Audio('assets/frontend/sounds/music.mp3');
        this.flipSound = new Audio('assets/frontend/sounds/flip.wav');
        this.matchSound = new Audio('assets/frontend/sounds/match.wav');
        this.victorySound = new Audio('assets/frontend/sounds/victory.wav');
        this.music.volume = 0.5;
        this.music.loop = true;
    }

    startMusic()
    {
        this.music.play();
    }

    stopMusic()
    {
        this.music.pause();
        this.music.currentTime = 0;
    }

    flip()
    {
        this.flipSound.play();
    }

    match()
    {
        this.matchSound.play();
    }

    victory()
    {
        this.stopMusic();
        this.victorySound.play();
    }
}
class MatchGame
{
	constructor(cards) {
		this.cardsArray = cards;
		this.cardForCheck = null;
		this.busy = false;
		this.matchedCards = [];
		this.flips = document.getElementById('flips');
        this.audio = new AudioController();
    }

    newGame()
    {
        bgNewGame();
        this.audio.startMusic();
        this.hideCards();
        this.matchedCards = [];
        this.busy = false;
        let xhr = new XMLHttpRequest();
        xhr.open('GET', `${apiUrl}/games/${gameId}/new_game`, true);
        xhr.onload = function()
        {
            let res = JSON.parse(this.response);
            // console.log('new game');
            localStorage.setItem('gameKey', res.game_key);
        };
        xhr.send();
    }

    flipCard(card, idx)
    {
        if(this.canFlipCard(card))
        {
            this.audio.flip();
            let res = this.getCard(idx);
            card.getElementsByClassName('card-value')[0].innerText = res.card;
            card.classList.add('visible');
            this.flips.innerText = res.flips;
            // console.log('flip card:',idx);
            // console.log('flips:',res.flips);
            if(!this.cardForCheck)
                this.cardForCheck = card;
            else
                this.checkCardMatch(card);
        }
    }

    canFlipCard(card)
    {
        return !this.busy && !this.matchedCards.includes(card) && card !== this.cardForCheck;
    }

    getCard(idx)
    {
        let xhr = new XMLHttpRequest();
        xhr.open('PUT', `${apiUrl}/games/${gameId}/fliping` , false);
        xhr.setRequestHeader('Content-Type','application/json; charset=UTF-8');
        xhr.send(JSON.stringify({game_key: this.gameKey(), idx: idx}));
        if (xhr.readyState == 4 && xhr.status == 200)
            return JSON.parse(xhr.response);
    }

    getCardValue(card)
    {
        return card.getElementsByClassName('card-value')[0].innerText;
    }

	checkCardMatch(card)
	{
        this.busy = true;
		let matchedCardsIndex = [];
        matchedCardsIndex.push(this.cardsArray.indexOf(card));
        matchedCardsIndex.push(this.cardsArray.indexOf(this.cardForCheck));

        let xhr = new XMLHttpRequest();
        xhr.open('PUT', `${apiUrl}/games/${gameId}/matching`, true);
        xhr.setRequestHeader('Content-Type','application/json; charset=UTF-8');
        let self = this;
        xhr.onload = function()
        {
            let res = JSON.parse(this.response);
            if(res.result)
                self.cardMatch(card, self.cardForCheck);
            else
                self.cardMismatch(card, self.cardForCheck);
            self.cardForCheck = null;
        }
        xhr.send(JSON.stringify({game_key: this.gameKey(), cards: matchedCardsIndex}));
	}

	cardMatch(card1, card2)
	{
        this.audio.match();
        this.busy = false;
	  	this.matchedCards.push(card1);
	  	this.matchedCards.push(card2);
	  	card1.classList.add('matched');
	  	card2.classList.add('matched');
        // console.log('card match');
        if(this.matchedCards.length === this.cardsArray.length)
        	this.endGame();
    }

    cardMismatch(card1, card2)
    {
    	this.busy = true;
    	setTimeout(() => {
    		card1.classList.remove('visible');
    		card2.classList.remove('visible');
    		this.busy = false;
    	}, 1000);
        // console.log('card mismatch');
    }

    hideCards()
    {
    	this.flips.innerText = 0;
    	this.cardsArray.forEach(card => {
    		card.classList.remove('visible');
    		card.classList.remove('matched');
    	});
    }

    endGame()
    {
        this.audio.victory();
        bgEndGame();
    	this.busy = true;
    	if(checkLogin())
    		this.recordGame();
    	localStorage.removeItem('gameKey');
        // console.log('end game');
    }

    recordGame()
    {
    	let xhr = new XMLHttpRequest();
    	let token = localStorage.getItem('token');
    	xhr.open('POST', `${apiUrl}/games/${gameId}/finish`, true);
    	xhr.setRequestHeader('Content-Type','application/json; charset=UTF-8');
    	xhr.setRequestHeader('Authorization', `Bearer ${token}`);
    	xhr.onload = function()
    	{
            let res = JSON.parse(this.response)
            if(res.my_best)
                document.getElementById('my-best').innerText = res.my_best;
            // console.log('record game');
	    };
	    xhr.send(JSON.stringify({game_key: this.gameKey()}));
	}

	gameKey()
	{
		return localStorage.getItem('gameKey');
	}
}