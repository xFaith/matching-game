<?php

namespace Backend;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class BackendServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(Router $router)
    {
        //
        $this->loadRoutesFrom(__DIR__.'/api.php');
        $this->loadViewsFrom(__DIR__.'/views', 'backend');
    }
}
