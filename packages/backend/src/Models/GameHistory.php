<?php

namespace Backend\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class GameHistory extends Model
{
    use HasFactory;
    protected $table = 'game_histories';
    protected $guarded = [];

    public function user()
    {
      return $this->belongsTo('Backend\Models\User','user_id');
    }
}
