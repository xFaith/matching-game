<?php

namespace Backend\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Game extends Model
{
    use HasFactory;
    protected $table = 'games';
    protected $guarded = [];
    protected $appends = ['global_best'];
    public $cacheTime = 360;

    public function histories()
    {
        return $this->hasMany('Backend\Models\GameHistory', 'game_id');
    }

    public function historiesByUserId($user_id)
    {
        return $this->hasMany('Backend\Models\GameHistory', 'game_id')->whereUserId($user_id);
    }

    public function userMinFlip($user_id)
    {
        return $this->historiesByUserId($user_id)->min('flips');
    }

    public function globalMinFlip()
    {
        return $this->histories()->min('flips');
    }

    public function getGlobalBestAttribute()
    {
        return $this->globalMinFlip();
    }

    public function isEvenNumber()
    {
        if(!is_numeric($this->deck))
            return false;
        return $this->deck%2 == 0?true:false;
    }

    public function createCard()
    {
        $cards = [];
        $last_number = $this->deck/2;

        for($i = 1; $i<=$last_number; $i++)
        {
            array_push($cards, $i, $i);
        }

        shuffle($cards);
        return $cards;
    }

    public function checkCardMatch($cards)
    {
        return $cards[$this->card_1] == $cards[$this->card_2];
    }

    public function checkGlobalNewBest()
    {
        $min = $this->globalMinFlip();
        if(!$min)
            return true;
        return $this->flips < $min;
    }

    public function checkMyNewBest($user_id)
    {
        $min = $this->globalMinFlip();
        if(!$min)
            return true;
        return $this->flips < $min;
    }

    public function setCache()
    {
        Cache::put($this->key, $this->cache, $this->cacheTime);
    }

    public function getCache()
    {
        return Cache::get($this->key);
    }

    public function deleteCache()
    {
        return Cache::forget($this->key);
    }
}