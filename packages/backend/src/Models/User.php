<?php

namespace Backend\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{
    use HasFactory, Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username',
        'email',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function histories()
    {
        return $this->hasMany('Backend\Models\GameHistory',"user_id");
    }

    public function historiesByGameId($game_id)
    {
        return $this->hasMany('Backend\Models\GameHistory',"user_id")->whereGameId($game_id);
    }

    public function userMinFlip($game_id)
    {
        return $this->historiesByGameId($game_id)->min('flips');
    }
}
