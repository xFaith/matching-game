<?php

namespace Backend\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth; 
use Illuminate\Http\Request;
use Backend\Models\Game;
use Backend\Events\NewBestEvent;

class GameController extends Controller
{
    //
    private $id;
    private $key;
    private $err_message = [
        'game key required',
        'game not found',
        'invalid card index',
        'only even number',
        'error game score',
        'only 2 cards',
        'error game score',
    ];

    public function __construct(Request $request)
    {

    }

    public function getGame(Game $game)
    {
    	return response()->json($game);
    }

    public function newGame(Game $game)
    {
        if(!$game->isEvenNumber())
            return $this->handleErr(500, $this->err_message[3]);

    	$game->key = uniqid();
    	$game->cache = [
    		'id' => $game->id,
    		'cards' => $game->createCard(),
            'matched_card' => [],
    		'flips' => 0,
    	];
        $game->setCache();
    	return response()->json(['game_key' => $game->key]);
    }

    public function flipCard(Request $request,Game $game)
    {
        if(!$request->game_key)
            return $this->handleErr(400, $this->err_message[0]);

        $game->key = $request->game_key;

        $cache = $game->getCache();
        if($cache == null)
            return $this->handleErr(500, $this->err_message[1]);

        $cache['flips'] = $flips = $cache['flips']+1;
        $game->cache = $cache;
        $game->setCache();

        $idx = $request->idx;
        $cards = $cache['cards'];
        if(!isset($cards[$idx]))
            return $this->handleErr(500, $this->err_message[2]);

        return response()->json(['flips' => $flips, 'card' => $cards[$idx]]);
    }

    public function checkMatch(Request $request,Game $game)
    {
        if(!$request->game_key)
            return $this->handleErr(400, $this->err_message[0]);
        $game->key = $request->game_key;

        if(count($request->cards) != 2)
            return $this->handleErr(400, $this->err_message[5]);

        $game->card_1 = $request->cards[0];
        $game->card_2 = $request->cards[1];
        $cache = $game->getCache();
        if($cache == null)
            return $this->handleErr(500, $this->err_message[1]);

        $cards = $cache['cards'];
        if(!isset($cards[$game->card_1]) || !isset($cards[$game->card_2]) || in_array($game->card_1, $cache['matched_card']) || in_array($game->card_2, $cache['matched_card']) || $game->card_1 == $game->card_2)
            return $this->handleErr(500, $this->err_message[2]);

        if($result = $game->checkCardMatch($cards))
        {
            array_push($cache['matched_card'], $game->card_1, $game->card_2);
            $game->cache = $cache;
            $game->setCache();
        }
        return response()->json(['result' => $result]);
    }

    public function finishGame(Request $request, Game $game)
    {
        if(!$request->game_key)
            return $this->handleErr(400, $this->err_message[0]);
        $game->key = $request->game_key;

        $cache = $game->getCache();
        if($cache == null)
            return $this->handleErr(500, $this->err_message[1]);

        if($cache['flips'] < $game->deck)
            return $this->handleErr(500, $this->err_message[4]);
        
        $input_data['flips'] = $cache['flips'];
        $input_data['user_id'] = $user_id = Auth::user()->id;

        $game->flips = $cache['flips'];
        $new_best = ['global_best' => false, 'my_best' => false];

        if($game->checkMyNewBest($user_id))
            $new_best['my_best'] = $game->flips;

        if($game->checkGlobalNewBest())
        {
            event(new NewBestEvent($game->flips));
            $new_best['global_best'] = true;
        }

        $game->histories()->create($input_data);
        // $game->deleteCache();

        return $new_best;
    }

    public function getGlobalBest(Game $game)
    {
        return response()->json(['global_best' => $game->globalMinFlip()]);
    }

    public function getMyBest(Game $game)
    {
        return response()->json(['my_best' => $game->userMinFlip(Auth::user()->id)]);
    }

    private function handleErr($status, $message = null)
    {
        return response()->json(['message' => $message], $status);
    }
}