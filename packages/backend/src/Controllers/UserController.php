<?php

namespace Backend\Controllers;

use Illuminate\Http\Request; 
use App\Http\Controllers\Controller; 
use Illuminate\Support\Facades\Auth; 
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Backend\Models\User; 
use Validator;

class UserController extends Controller 
{
    use AuthenticatesUsers;
/** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 

    public function __construct(Request $request) { 
    }

    public function login(Request $request)
    {
        if(Auth::attempt($request->only('username','password'))){ 
            $user = Auth::user();
            // $userTokens = $user->tokens()->delete();
            $result = $this->createToken($user);
            return response()->json($result);
        } 
        else
        { 
            return response()->json(['error'=>'Unauthorised'], 401); 
        } 
    }

    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username' => 'required|unique:users',
            'password' => 'required|confirmed',
        ]);

        if ($validator->fails())
        {
            return response()->json(['error' => $validator->errors()], 400);
        }

        $input = $request->all();
        $input['password'] = bcrypt($input['password']);
        $user = User::create($input);
        $success = $this->createToken($user);
        return response()->json(['success' => $success]);
    }
/** 
     * Register api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    
    public function createToken($user)
    { 
        $result['token'] =  $user->createToken('MyApp')->accessToken; 
        $result['username'] =  $user->username;
        return $result;
    }
    
    protected function guard()
    {
        return Auth::guard('api');
    }
}