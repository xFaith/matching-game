<?php

Route::group(['namespace' => 'Backend\Controllers', 'prefix' => 'api'], function(){

	Route::get('login','UserController@login')->name('login');
	Route::post('login','UserController@login');
	Route::post('register','UserController@register');

	Route::group(['middleware' => 'bindings'], function(){
		Route::get('games/{game}','GameController@getGame')->name('game.get');
		Route::get('games/{game}/new_game','GameController@newGame')->name('game.new');
		Route::put('games/{game}/fliping','GameController@flipCard')->name('game.card.show');
		Route::put('games/{game}/matching','GameController@checkMatch')->name('game.card.match');
		Route::get('games/{game}/global_best','GameController@getGlobalBest')->name('game.global_best');
	});

	Route::group(['middleware' => ['auth:api','bindings']], function ()
	{
	    Route::get('games/{game}/my_best','GameController@getMyBest')->name('game.my_best');
	    Route::post('games/{game}/finish','GameController@finishGame')->name('game.finish');
	});
});