<?php

namespace Frontend\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class GameController extends Controller
{
    public function __construct(Request $request)
    {
    }
    public function index()
    {
        $params['game_id'] = 1;
        $params['config'] = config('custom');
        return view('frontend::master', $params);
    }
}