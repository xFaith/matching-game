<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<link rel="stylesheet" type="text/css" href="{{asset('css/style.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/cloud.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/login.css')}}">
	<link rel="stylesheet" type="text/css" href="{{asset('css/card.css')}}">
	<title>Match-The-Cards</title>
</head>
<body>
	<div id="overlay" class="overlay visible">
		@include('frontend::login')
		@include('frontend::register')
	</div>
	<div class="background-wrap visible">
		@for($i=1;$i<=6;$i++)
		<div class="cloud x{{$i}}">
	        <div class="cloud-item"></div>
	    </div>
		@endfor
	</div>
	<div class="background-wrap">
		<div class="backgroud-img"></div>
	</div>
	<div class="game-container visible">
		<div class="game-info-container">
				<div class="user-info">Play as Guest</div>
			<!-- </div> -->
			<div class="game-info">
				<div class="game-info-item">My Best <span id="my-best">-</span></div>
				<div class="game-info-item">Global Best <span id="global-best">-</span></div>
				<div class="game-info-item">Flips <span id="flips">0</span></div>
			</div>
		</div>
		<div class="game-wrap">
			<div class="game-wrap-item">
				<div class="button-box">
					<button id="new-game" class="btn btn-primary" type="button">New Game</button>
				</div>
			</div>
			<div id="card-container" class="game-wrap-item">
				
			</div>
		</div>
	</div>
</body>
<script src="{{asset('js/script.js')}}"></script>
<script src="{{asset('js/match_game.js')}}"></script>
<script src="{{asset('js/auth.js')}}"></script>
@include('frontend::pusher', ['config' => $config])
<script type="text/javascript">
	var apiUrl = '{{$config['api_url']}}';
	var gameId = '{{$game_id}}';
</script>
</html>