		<div class="login-box login-form active">
			<h2>Login</h2>
			<form id="login-form">
				<div class="user-box">
					<input type="text" name="username" required>
					<label>Username</label>
					<div class="user-border"></div>
				</div>
				<div class="user-box">
					<input type="password" name="password" required>
					<label>Password</label>
					<div class="user-border"></div>
				</div>
			</form>
			<div class="button-box">
				<button class="btn btn-primary" type="button" onclick="login()">Submit</button>
				<button class="btn btn-primary" type="button" onclick="registerForm()">Register</button>
				<button class="btn btn-primary" type="button" onclick="ready()">Play as Guest</button>
			</div>
			<div class="error-wrap">
			</div>
		</div>	