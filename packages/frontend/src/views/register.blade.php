		<div class="login-box register-form">
			<h2>Register</h2>
			<form id="register-form">
				<div class="user-box">
					<input type="text" name="username" required>
					<label>Username</label>
					<div class="user-border"></div>
				</div>
				<div class="user-box">
					<input type="password" name="password" required>
					<label>Password</label>
					<div class="user-border"></div>
				</div>
				<div class="user-box">
					<input type="password" name="password_confirmation" required>
					<label>Re-Password</label>
					<div class="user-border"></div>
				</div>
			</form>
			<div class="button-box">
				<button class="btn btn-primary" type="button" onclick="register()">Submit</button>
				<button class="btn btn-primary" type="button" onclick="loginForm()">Back</button>
			</div>
			<div class="error-wrap">
			</div>
		</div>	