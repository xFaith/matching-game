<script src="https://js.pusher.com/7.0/pusher.min.js"></script>
<script>
	// Enable pusher logging - don't include this in production
	Pusher.logToConsole = true;

	var pusher = new Pusher('{{$config['pusher']['key']}}', {cluster: '{{$config['pusher']['cluster']}}'});
	var channel = pusher.subscribe('my-channel');

	channel.bind('my-event', function(data) {
		let res = JSON.parse(JSON.stringify(data));
		document.getElementById('global-best').innerText = res.global_best;
	});
</script>