<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GameHistoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('game_histories')->insert([
            [
                'game_id' => 1,
                'user_id' => 1,
                'flips' => 30,
            ],
            [
                'game_id' => 1,
                'user_id' => 1,
                'flips' => 36,
            ],
            [
                'game_id' => 1,
                'user_id' => 1,
                'flips' => 48,
            ]
        ]);
    }
}
