<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class GameTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('games')->insert([
            'title' => 'เกมจับคู่ไพ่ 12 ใบ',
            'description' => 'เกมจับคู่ไพ่ โดยมีไพ่ทั้งหมด 12 ใบ หน้าไพ่เป็นหมายเลข 1-6 หมายเลขละ 2 ใบ',
            'deck' => 12,
        ]);
    }
}
